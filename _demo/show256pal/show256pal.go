package main

import (
	"fmt"
	"strings"

	"bitbucket.org/gmelchett/textcolor"
)

func center(text string, size int) string {
	if len(text) > size {
		return text[:size]
	}
	padding := size - len(text)
	startpadding := padding / 2
	endpadding := startpadding
	if padding&1 == 1 {
		endpadding++
	}
	return strings.Repeat(" ", startpadding) + text + strings.Repeat(" ", endpadding)
}

func main() {

	fmt.Println(center("Standard colors", 90) + center("High-intensity colors", 90))

	for i := 0; i < 16; i++ {
		c := textcolor.Black
		if i < 7 {
			c = textcolor.White
		}
		if i == 8 {
			fmt.Printf(textcolor.Reset + "    ")
		}

		t := textcolor.Color(c, i) + center(fmt.Sprintf("%d", i), 180/16)
		fmt.Printf(t)
	}
	fmt.Println(textcolor.Reset + "\n" + center("216 colors", 180))

	for i := 16; i < 232; i++ {
		c := textcolor.Black
		if (i-16)%36 < 18 {
			c = textcolor.White
		}
		t := textcolor.Color(c, i) + fmt.Sprintf(" %03d ", i) + textcolor.Reset
		fmt.Printf(t)
		if (i-16)%36 == 35 {
			fmt.Printf("\n")
		}
	}

	fmt.Printf("\n" + strings.Repeat(" ", 6*5))

	for i := 232; i < 256; i++ {
		c := textcolor.Black
		if (i - 232) < 12 {
			c = textcolor.White
		}
		t := textcolor.Color(c, i) + fmt.Sprintf(" %03d ", i) + textcolor.Reset
		fmt.Printf(t)
	}

	fmt.Println(textcolor.Reset)
}
