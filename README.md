# textcolor

Colorize your console application!

Simple go package that lets you easily colorize your console application.

![alt text](screenshot.png)
