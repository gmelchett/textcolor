package textcolor

import "fmt"

const (
	Black = iota
	Red
	Green
	Yellow
	Blue
	Magenta
	Cyan
	White
	BrightBlack
	BrightRed
	BrightGreen
	BrightYellow
	BrightBlue
	BrightMagenta
	BrightCyan
	BrightWhite
)

var ItalicOn = "\033[3m"
var ItalicOff = "\033[23m"
var UnderLineOn = "\033[4m"
var UnderLineOff = "\033[24m"
var BlinkOn = "\033[5m"
var BlinkOff = "\033[25m"
var Reset = "\033[0m"
var BoldOn = "\033[1m"
var BoldOff = "\033[21m"
var StrikeThroughOn = "\033[9m" // Not always working
var StrikeThroughOff = "\033[29m"

const fg1Offset = 30
const bg1Offset = 40
const bg2Offset = 100

func Fg(fg int) string {

	if fg < BrightBlack {
		return fmt.Sprintf("\033[%dm", fg+fg1Offset)
	}
	if fg <= BrightWhite {
		return fmt.Sprintf("\033[%d;1m", fg+fg1Offset-BrightBlack)
	}
	return fmt.Sprintf("\033[38;5;%dm", fg)
}

func Bg(bg int) string {
	if bg < BrightBlack {
		return fmt.Sprintf("\033[%dm", bg+bg1Offset)
	}
	if bg <= BrightWhite {
		return fmt.Sprintf("\033[%dm", bg+bg2Offset-BrightBlack)
	}
	return fmt.Sprintf("\033[48;5;%dm", bg)
}

func BgRGB(r, g, b int) string {
	return fmt.Sprintf("\033[48;2;%d;%d;%dm", r, g, b)
}

func FgRGB(r, g, b int) string {
	return fmt.Sprintf("\033[38;2;%d;%d;%dm", r, g, b)
}

func Color(fg, bg int) string {
	return Bg(bg) + Fg(fg)
}

func ColorRGB(fr, fg, fb, br, bg, bb int) string {
	return BgRGB(br, bg, bb) + FgRGB(fr, fg, fb)
}

func Text(fg, bg int, txt string) string {
	return Color(fg, bg) + txt + Reset
}
